#!/usr/bin/env python


from distutils.core import setup
from fnmatch        import fnmatch
import os

def listfiles(*dirs):
        dir, pattern = os.path.split(os.path.join(*dirs))
        return [os.path.join(dir, filename)
        for filename in os.listdir(os.path.abspath(dir))
           if filename[0] != '.' and fnmatch(filename, pattern)]

setup(
        name             = 'resm',
        version          = '0.1',
        description      = 'Resource manager as a Test Challange',
        long_description = "Application written as a Test Challence for a developer position in Machine Zone",
        author           = 'Ivan GUSHCHIN <gunja>',
        author_email     = 'konara@ya.ru',
        url              = 'http://bitbucket.org/gunja/resm_test',
        platforms        = 'linux',
        license          = 'GPL-2.0',
        scripts          = ['resm.py'],
        data_files       = [
                           ('/usr/share/resm/', [ 'resm.py' ] ),
                           ],
        )

