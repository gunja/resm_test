#!flask/bin/python
import os
import resm
import unittest

class TestCaseSettingUp(unittest.TestCase):
     def setUp(self):
          resm.port_val =0
          resm.poolSize =0
          self.app = resm.app.test_client()

     def tearDown(self):
          resm.port_val =0
          resm.poolSize =0

     def test_values(self):
          resm.main( ['-p','8900', '-s', '20'] )
          #print 'port=' + str( resm.port_val)
          assert resm.port_val == 8900
          #print 'port_val=' + str( resm.port_val)
          assert resm.poolSize == 20
          self.app = resm.app.test_client()
          rv = self.app.get('/list')
          assert 'r19' in rv.data

     def test_default_values(self):
          resm.main( [] )
          assert resm.port_val == 8810
          assert resm.poolSize == 11
          rv = self.app.get('/list')
          #print 'rv11=' + rv.data
          assert 'r11' in rv.data

     def test_port_(self):
          resm.main( ['--port=95'] )
          assert resm.port_val == 95
          assert resm.poolSize == 11
          rv = self.app.get('/list')
          assert 'r11' in rv.data

     def test_port_2(self):
          resm.main( ['-p','96'] )
          assert resm.port_val == 96
          assert resm.poolSize == 11
          rv = self.app.get('/list')
          assert 'r11' in rv.data

     def test_pool(self):
          resm.main( ['-s','90'] )
          assert resm.port_val == 8810
          assert resm.poolSize == 90
          rv = self.app.get('/list')
          assert 'r90' in rv.data

     def test_pool_2(self):
          resm.main( ['--size=93'] )
          assert resm.port_val == 8810
          assert resm.poolSize == 93
          rv = self.app.get('/list')
          assert 'r93' in rv.data

class TestCase_list(unittest.TestCase):
    def setUp(self):
          resm.port_val =0
          resm.poolSize =0
          self.app = resm.app.test_client()
          resm.main([])

    def tearDown(self):
        self.app.get('/reset')

    def test_list(self):
        resm.alloced =  {u'r20': 'Alice', u'r21': 'alice', u'r22': 'bob'}
        rv = self.app.get('/list')
        assert rv.status_code == 200 and '"r20": "Alice"' in rv.data and \
               '"r01"' in rv.data and '"r11"' in rv.data
        rv = self.app.get('/list/alise')
        assert rv.status_code  == 200 and rv.data == '[]'
        rv = self.app.put('/list/alice')
        assert rv.status_code == 400 and rv.data == 'Bad request.'
        rv = self.app.get('/list/Alice')
        assert rv.status_code == 200 and 'r20' in rv.data
        rv = self.app.get('/list/alice')
        assert rv.status_code == 200 and 'r21' in rv.data and 'r20' not in rv.data
        rv = self.app.get('/list/')
        assert rv.status_code == 400
        rv = self.app.get('/list')
        assert rv.status_code == 200 and '"r20": "Alice"' in rv.data and '"r01"' in rv.data \
            and '"r11"' in rv.data
        rv = self.app.get('/reset')
        assert rv.status_code == 204
        rv = self.app.get('/list')
        assert rv.status_code == 200 and '"r20": "Alice"' not in rv.data and '"r01"' in rv.data \
            and '"r11"' in rv.data


class TestCase_whole(unittest.TestCase):
    def setUp(self):
        self.app = resm.app.test_client()
        resm.main('')

    def tearDown(self):
        self.app.get('/reset')

    def test_allocate(self):
        rv = self.app.get('/allocate')
        assert 400 == rv.status_code
        assert 'Bad request.' in rv.data
        rv = self.app.put('/allocate')
        assert 400 == rv.status_code
        rv = self.app.put('/allocate/alice')
        assert 400 == rv.status_code
        rv = self.app.get('/allocatee/alice')
        assert 400 == rv.status_code
        rv = self.app.get('/list')
        #print rv.data
        rv = self.app.get('/allocate/alice')
        #print 'rv.code='+ str(rv.status_code) + 'rv.data='+rv.data
        assert 201 == rv.status_code
        #assert 'r10' in rv.data



    def test_deallocate(self):
        rv = self.app.get('/deallocate/r1')
        assert rv.status_code == 404 and rv.data == 'Not allocated.'
        rv = self.app.get('/deallocate')
        assert rv.status_code == 400 and rv.data == 'Bad request.'
        rv = self.app.put('/deallocate')
        assert rv.status_code == 400 and rv.data == 'Bad request.'
        rv = self.app.get('/allocate/alice')
        val = rv.data
        rv = self.app.get('/allocate/alice')
        val2 = rv.data
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        self.app.get('/allocate/alice')
        rv = self.app.get('/allocate/alice')
        assert rv.status_code == 201 and 'r' in rv.data
        rv = self.app.get('/allocate/alice')
        assert rv.status_code == 503 and 'Out of resources.' in rv.data
        rv = self.app.get('/deallocate/'+val)
        assert rv.status_code == 204
        rv = self.app.put('/deallocate/'+val2 )
        assert rv.status_code == 400 and rv.data == 'Bad request.'
        return

    def test_list(self):
        #print 'test List'
        rv = self.app.get('/allocate/Alice')
        #print 'rv.code='+str(rv.status_code)
        assert 201 == rv.status_code
        val = rv.data
        rv = self.app.get('/list')
        assert 200 == rv.status_code
        assert val in rv.data
        rv = self.app.put('/list')
        assert 400 == rv.status_code
        rv = self.app.get('/list/alice')
        assert 200 == rv.status_code
        #print 'list/alice=' + rv.data
        assert '[]' == rv.data
        rv = self.app.get('allocate/alice')
        val1 = rv.data
        rv = self.app.get('allocate/alice')
        val2 = rv.data
        rv = self.app.get('/list/alice')
        assert val1 in rv.data and val2 in rv.data

        self.app.get('/allocate/Python')
        rv = self.app.get('/list/Python')
        assert 200 == rv.status_code
        assert 'r' in rv.data
        rv = self.app.get('/list/python')
        assert 200 == rv.status_code
        assert '[]' == rv.data

    def test_reset(self):
        #print 'test Reset'
        rv = self.app.get('/reset/')
        assert rv.status_code == 400
        rv = self.app.get('/reset/alice')
        assert 400 == rv.status_code
        rv = self.app.put('/reset')
        #print 'rv.status =' + str(rv.status_code )
        assert 400 == rv.status_code
        rv = self.app.get('/reset')
        assert 204 == rv.status_code
        assert len( rv.data) == 0

if __name__ == '__main__':
    unittest.main()
