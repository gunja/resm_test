#!flask/bin/python

from flask import Flask, jsonify,make_response
import sys, getopt
from threading import Lock

app = Flask(__name__)

alloced = dict()
dealloced = []

poolSize = 0
port_val = 0
host_val = ''
allocedLock = Lock()
deallocedLock = Lock()
keep_console = False

@app.route('/allocate/<string:user>', methods=['GET'])
def allocate_user(user):
    global dealocated
    global alloced
    if dealloced:
        with deallocedLock:
           val = dealloced.pop()
        alloced[val]= user
        return val,201
    else:
        return 'Out of resources.', 503

@app.route('/list', methods=['GET'])
def get_resources():
    return jsonify( {'allocated':alloced, 'deallocated':dealloced } )


@app.route('/deallocate/<string:resource>', methods=['GET'])
def dealocate(resource):
    global dealloced
    global alloced
    try:
       with allocedLock:
           del alloced[resource]
       with deallocedLock:
           dealloced.append(resource)
       return '', 204
    except KeyError:
        return 'Not allocated.', 404

@app.route('/list/<string:name>', methods=['GET'])
def get_resource(name):
    result2 = [ v for v,k in alloced.items() if k == name ]
    if result2:
        return '["' +'","'.join(result2) + '"]'
    else:
        return '[]'
       

@app.route('/reset', methods=['GET'])
def get_reset():
    with allocedLock:
        lis = alloced.keys()
        alloced.clear()
    with deallocedLock:
        dealloced.extend(lis)
    return "", 204

@app.errorhandler(404)
def not_found(error):
   return 'Bad request.', 400

@app.errorhandler(405)
def bad_method(error):
   return 'Bad request.', 400


def show_help():
    print 'resm.py - simple REST application example'
    print 'resm.py [-h|--help] will show this help and exit'
    print 'resm.py [{-c |--config=}configfile] [{-p|--port=} portnum] [{-s|--size=} poolsize] [-d] , where'
    print '   configfile - path to file containing configuration parameters. Command line options override configuration file'
    print '   portnum    - port number which will be used for accepting connections. Default value is 8810'
    print '   poolsize   - number of resources to be created. Default value is 11'
    print ' -d option will keep service from detaching from terminal ( no daemon mode)'

def load_config(config_file):
    return

def init_pool( poolSize ):
    global dealloced
    if dealloced:
        dealloced = ()
    maxLen = len(str(poolSize))
    dealloced = [ u'r{0:0{width}}'.format( number, width=maxLen ) for number in range(1, poolSize+ 1) ]

def main( argv):
    host_val_ = '0.0.0.0'
    port_val_ = 8810
    global port_val
    global poolSize
    poolSize_ = 11
    config_file = ''
    try:
        opts, args = getopt.getopt(argv,"hc:p:s:d",["config=","port=","help","size="])
    except getopt.GetoptError:
        show_help
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
           show_help
           sys.exit()
        elif opt in ("-c", "--config"):
            config_file = arg
        elif opt in ("-p", "--port"):
            port_val_ = int(arg)
        elif opt in ("-s", "--size"):
            poolSize_ = int(arg)
        elif opt in ("-d"):
            keep_console = True
    if len (config_file) > 0:
        load_config( config_file )
    if port_val <=0:
        port_val = port_val_
    if poolSize <=0:
        poolSize = poolSize_
    host_val = host_val_
    init_pool( poolSize )

if __name__ == "__main__":
    main(sys.argv[1:])
    app.run(host=host_val , port=port_val, debug=keep_console )

